#!/bin/bash

echo "Hello from v1" > /var/www/html/index.html
echo "$COMMIT_SHA" > /var/www/html/git
curl "https://www.w3schools.com/tags/demo_icon.gif" -o /var/www/html/favicon.ico

/usr/sbin/httpd -D BACKGROUND
tail -f /etc/httpd/logs/access_log
