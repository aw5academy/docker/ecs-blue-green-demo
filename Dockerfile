FROM amazonlinux:latest

RUN yum install -y httpd
COPY start.sh /start.sh
RUN chmod +x /start.sh
EXPOSE 80
CMD ["/start.sh"]
