#!/bin/bash

MAX_POLL_WAIT="1800"

verifyEnvVars() {
  if [ ! "$BUCKET_NAME" ]; then
    printf "No BUCKET_NAME env var!\n"
    exit 1
  fi
  if [ ! "$PIPELINE_EXECUTION_ID" ]; then
    printf "No PIPELINE_EXECUTION_ID env var!\n"
    exit 1
  fi
  if [ ! "$PIPELINE_NAME" ]; then
    printf "No PIPELINE_NAME env var!\n"
    exit 1
  fi
  if [ ! "$TEST_URL" ]; then
    printf "No TEST_URL env var!\n"
    exit 1
  fi
}

pollForDeploymentHookDetails() {
  S3_OBJECT_PREFIX="$BUCKET_NAME/pipelineExecutions/$PIPELINE_NAME"
  local poll_wait_time=0
  while [[ $poll_wait_time -lt $MAX_POLL_WAIT ]]; do
    if [[ "`aws s3 ls s3://$S3_OBJECT_PREFIX/ |grep $PIPELINE_EXECUTION_ID`" == *"$PIPELINE_EXECUTION_ID" ]]; then
      return 0
    else
      sleep 10
      poll_wait_time=$(($poll_wait_time + 10))
    fi
  done
  printf "Poll for deployment hook details at s3://$S3_OBJECT_PREFIX/ timed out after ${MAX_POLL_WAIT}s!\n"
  exit 1
}

getDeploymentHookDetails() {
  aws s3 cp s3://$S3_OBJECT_PREFIX/$PIPELINE_EXECUTION_ID .
  source $PIPELINE_EXECUTION_ID
}

verifyDeploymentHookDetails() {
  if [ ! "$DEPLOYMENT_ID" ]; then
    printf "No DEPLOYMENT_ID found in hook details!\n"
    exit 1
  fi
  if [ ! "$HOOK_EXECUTION_ID" ]; then
    printf "No HOOK_EXECUTION_ID found in hook details!\n"
    exit 1
  fi
}

waitForDeploymentToRegisterWithTestListener() {
  local max_wait="120"
  local wait_time=0
  while [[ $wait_time -lt $max_wait ]]; do
    curl -H 'Cache-Control: no-cache' $TEST_URL/git -o response.html
    if grep -q "$CODEBUILD_RESOLVED_SOURCE_VERSION" "response.html"; then
      return 0
    else
      sleep 5
      wait_time=$(($wait_time + 5))
    fi
  done
  printf "Deployed app not responding as expected!\n"
  exit 1
}

performTests() {
  curl -H 'Cache-Control: no-cache' $TEST_URL -o response.html
  if grep -q "Hello from v1" "response.html"; then
    TEST_RESULT="Succeeded"
  else
    TEST_RESULT="Failed"
  fi
}

putHookExecutionStatus() {
  aws deploy put-lifecycle-event-hook-execution-status \
    --deployment-id "$DEPLOYMENT_ID" \
    --lifecycle-event-hook-execution-id "$HOOK_EXECUTION_ID" \
    --status "$TEST_RESULT" \
    --region $AWS_REGION
}

exitFromCodeBuild() {
  if [ "$TEST_RESULT" == "Succeeded" ]; then
    exit 0
  else
    exit 1
  fi
}

verifyEnvVars
pollForDeploymentHookDetails
getDeploymentHookDetails
verifyDeploymentHookDetails
waitForDeploymentToRegisterWithTestListener
performTests
putHookExecutionStatus
exitFromCodeBuild
